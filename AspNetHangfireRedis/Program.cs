using AspNetHangfireRedis.Services;
using Hangfire;
using Hangfire.Console;
using Hangfire.Redis.StackExchange;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddHangfire(options => {
    var connectionString = builder.Configuration.GetValue<string>("RedisConnection");
    var redis = ConnectionMultiplexer.Connect(connectionString);
    options.UseRedisStorage(redis, options: new RedisStorageOptions { Prefix = $"HANG_FIRE"});
    options.UseConsole();
});
builder.Services.AddHangfireServer();
builder.Services.AddHostedService<MonitorServices>();
builder.Services.AddControllers();
var app = builder.Build();
app.MapHangfireDashboard("/hangfire");
app.Run();
