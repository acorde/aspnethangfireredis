﻿
using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using System.Runtime.CompilerServices;

namespace AspNetHangfireRedis.Services
{
    public class MonitorServices : IHostedService
    {
        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AddJobHangFire();
        }

        private async Task AddJobHangFire()
        {
            //Agendado
            BackgroundJob.Schedule(() => Print("Agendamento", null), TimeSpan.FromSeconds(5));
            //Em fila
            var jobId =BackgroundJob.Enqueue("teste", () => Print("Teste in queue",null));
            //Rodar um processo após termino do processo Pai
            BackgroundJob.ContinueJobWith(jobId, () => Print($"Fila filha de {jobId}", null));
            //Recorrente
            RecurringJob.AddOrUpdate("RecurringJob", () => Print("", null), MinuteInterval(5));
        }

        public void Print(string message, PerformContext? context) 
        {
            context.WriteLine(message);
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }
    }
}
